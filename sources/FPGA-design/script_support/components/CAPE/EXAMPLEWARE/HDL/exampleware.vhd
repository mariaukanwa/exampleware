-- Make an led slither like a snake, by rotating left/right the bits on a bus

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity snake is
port (
	clk			: in  std_logic;
	reset_n		: in  std_logic;
	leds		: out std_logic_vector(10 downto 0));
end snake;

architecture behavior of snake is

constant max_clk_count: integer := 50000000;
--constant max_clk_count	: integer := 3;	 -- for debug, snake shift every 3 clock instead of every 5mill clocks
signal count_clk_r		: unsigned(25 downto 0) := to_unsigned(0, 26);

signal leds_r	: std_logic_vector(10 downto 0) := "00011111000";
signal dir_r 	: std_logic := '0';   	-- tells the direction left/right
signal tick 	: std_logic := '0';
 
begin
	leds <= leds_r;
	
	tick <= '1' when (count_clk_r = max_clk_count) else  '0';
	
	counter_proc: process(clk)
		begin
			if(rising_edge(clk)) then
				if(reset_n = '0' or tick = '1') then
					count_clk_r <= (others => '0');
				else
					count_clk_r <= count_clk_r + 1;
				end if;
			end if;
		end process counter_proc;
		
	count_proc: process(clk)
		begin
			if(rising_edge(clk)) then
				if(reset_n = '0') then
					dir_r   <= not dir_r;
				else
					if (tick = '1') then
						if (dir_r = '1') then
							leds_r <= leds_r(9 downto 0) & leds_r(10);  -- rotate left
						else
							leds_r <= leds_r(0) & leds_r(10 downto 1);	-- rotate right
						end if;
					end if;
				end if;
			end if;
	end process count_proc;

end behavior;
